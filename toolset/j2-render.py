#!/usr/bin/env python

import json
import argparse
from jinja2 import Template


def render_template(buffer, **vars):
    return Template(buffer).render(**vars)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--template", required=True, help="File with Jinja2 instructions")
    parser.add_argument("--json-vars", help="File with variables in JSON format. Might be used multiple times")
    parser.add_argument("--output", required=False, help="Where to save the rendered file")
    parser.add_argument("--stdout", action='store_true', help="Print the output to stdout")

    args = parser.parse_args()

    vars = {}
    with open(args.json_vars, "r") as fd:
        vars = json.load(fd)

    try:
        j2_template = open(args.template, 'r').read()
        output = render_template(j2_template, **vars)
        print(args.output)
        if args.output is not None:
            open(args.output, 'w').write(output)
        if args.stdout:
            print(output)
    except Exception as err:
        print("Error: " + err.message)
        exit(1)

    exit(0)


#!/usr/bin/env groovy

/*
    Jenkins requires following plugins being installed:
        - Pipeline Utility Steps


*/


def toolset_config_text = '''
{
    "root_dir": "toolset",
    "tools": [],
    "docker": {
        "labels": {
            "Name": "ToolSet",
            "Owner": "Data Analytics",
            "Cost-Center": "R&D",
            "Environment": "Dev"
        }
    }
}
'''

def toolset_localdir = 'toolset'
def toolset_config // It'll be a JSON/Map object from toolset_config_text


pipeline {
agent any
    stages {
        stage('Init') {
            options {
                skipDefaultCheckout()
            }
            steps {
                clone_repo('https://bitbucket.org/dmadm2008/learning-jenkins.git', 'version-2', './' )
                dir( toolset_localdir ) {
                    script {
                        try {
                            toolset_config = new groovy.json.JsonSlurperClassic().parseText(toolset_config_text)
                        } catch ( err ) {
                            print_log( err )
                        }
                        print_log("Initial configuration: ${toolset_config}")
                        toolset_config.tools = find_tools()
                    }
                    stash 'Toolset_dir'
                }
            }
        }
        stage("Build Toolset") {
            steps {
                script {
                    unstash 'Toolset_dir'
                    exec_cmd( toolset_config, 'build_cmd' )
                    //dir( toolset_localdir ) {
                        //build_toolset( toolset_config )
                    //}
                }
            }
        }
        stage("Test Toolset") {
            steps {
                script {
                    exec_cmd( toolset_config, 'test_cmd' )
                }
            }
        }
        stage("Clean") {
            steps {
                script {
                    exec_cmd( toolset_config, 'clean_cmd' )
                    /*
                    dir( toolset_localdir ) {
                        clean_tools( toolset_config.root_dir, toolset_config.tools )
                    }
                    */
                }
            }
        }
    }
    post {
        always {
            // save_build_version()
            // cleanWs()
            echo "Clean up workspace"
        }
    }
}

def exec_cmd( Object toolset_config, String command_kind, String options="" ) {
    println "Run the exec_cmd function"
    sh "pwd; ls -l"
    def tasks = [:]
    dir( toolset_config.root_dir ) {
        for ( tool_ in toolset_config.tools ) {
            def tool = tool_
            dir( tool.root_dir ) {
                println "Here's the tools dir"
                sh "pwd; ls -l"
                if ( tool.get(command_kind) != null ) {
                    println "Going to execute " + tool[command_kind] + " on " + tool.name
                    tasks[tool.name] = {
                        sh script: tool[command_kind] + " " + options
                    }
                }
            }
        }
    }
    parallel tasks
}


def find_tools() {
    def configs = []
    def files = findFiles(glob: '**/config.json')
    for ( f in files ) {
        print_log("Found a tool config: \"${f.name}\" in \"${f.directory}\" by path \"${f.path}\"")
        def data = readJSON(file: f.path)
        if ( data.enabled == "true" ) {
            configs << data
        }
    }
    print_log("Toolset confs: ${configs}")
    return configs
}


def clone_repo(String url, String branch, String localDir) {
    checkout([
        $class: 'GitSCM',
        branches: [[ name: '*/' + branch ]],
        doGenerateSubmoduleConfigurations: false,
        extensions: [[ $class: 'RelativeTargetDirectory', relativeTargetDir: localDir ]],
        submoduleCfg: [],
        userRemoteConfigs: [[ url: url ]]
    ])
}


def clean_tools(String toolsDir, Object tools, String nodeLabel="") {
    def tasks = [:]
    dir( toolsDir ) {
        for ( tool_ in tools ) {
            def tool = tool_
            node( label: nodeLabel ) {
                tasks[tool.name] = {
                    dir( tool.root_dir ) {
                        // sh script: "${tool.cleanall_cmd} ImageName=${tool.image.name} ImageTag=${tool.image.tag}"
                        println ("Clean all")
                    }
                }
            }
        }
    }
    parallel tasks
}


/**
 * @param tool - a json configuration of a tool to build
 * @return nothing
 */
def build_tool(Object tool) {
    try {
        print_log( "Current config is ${tool}" )
        print_log( "Building tool: ${tool.name}" )
        dir( tool.root_dir ) {
            print_log( "The tool's dir before build" )
            sh "pwd; ls -la"
            // save the image details to Dockerfile.j2-vars
            writeFile file: 'Dockerfile.j2-vars', text: groovy.json.JsonOutput.toJson(tool.docker)
            def result = sh script: "${tool.build_cmd} ImageName=${tool.image.name} ImageTag=${tool.image.tag}", returnStatus: true
            print_log( "Build result is " + ( result == 0 ? "SUCCESS" : "FAILED" ))
            print_log( "The tool's dir after build" )
            sh "pwd; ls -la"
            if ( tool.get('library') ) {
                def library = load(tool.library)
                library.example()
            }
            sh tool.clean_cmd
        }
    } catch (err) {
        error("Building tool \"${tool.name}\" failed with err: ${err}")
    }
}

// def build_toolset(String toolset_conf_text, String nodeLabel="") {
def build_toolset(Object toolset_conf, String nodeLabel="") {
    print_log("The whole configuration is ${toolset_conf}")
    def tools = [:]
    def build_ok = true
    dir(toolset_conf.root_dir) {
        for ( tool_ in toolset_conf.tools ) {
            def tool = tool_ // to prevent sharing the same data among the parallel tasks
            print_log("Checking conf is ${tool}")
            node ( label: nodeLabel ) {
                if ( tool.enabled == "true" ) {
                    tool.docker.labels = tool.docker.labels + toolset_conf.docker.labels
                    tool.docker.labels.Name = tool.name
                    tools["${tool.name}"] = {
                        build_tool(tool)
                    }
                }
            }
        }
    }
    parallel tools
    return build_ok
}


def print_error(String msg) {
    print_log(' >>> ERROR: ' + msg)
}

def print_log(String msg) {
    println msg
}




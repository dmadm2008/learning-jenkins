#!/bin/bash


# This check works only for declarative pipelines which includes the "pipeline {}" block.

# CSRF protection should be enabled in Jenkins

JENKINS_URL=http://192.168.3.10:8080
API_TOKEN=${JENKINS_API_TOKEN:-}
API_USER=${JENKINS_API_USER:-}
JENKINS_CRUMB=`curl -k -s -u "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" "${JENKINS_URL}/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)"`
if [ x"${JENKINS_CRUMB%:*}" != x"Jenkins-Crumb" ]; then
    echo " >>> Jenkins Autho token has not been received."
    exit 1
fi
export JENKINS_CRUMB
response=$(curl -k -X POST -s -u "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" -H $JENKINS_CRUMB -F "jenkinsfile=<Jenkinsfile" $JENKINS_URL/pipeline-model-converter/validate)

if [ x"$response" == x"Jenkinsfile successfully validated." ]; then
    echo $response
    exit 0
else
    echo " >>> Jenkinsfile contains errors."
    echo $response
    exit 1
fi


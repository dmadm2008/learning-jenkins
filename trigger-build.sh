#!/bin/bash

set -x

# CSRF protection should be enabled in Jenkins

JENKINS_URL=http://192.168.3.10:8080
API_TOKEN=${JENKINS_API_TOKEN:-}
API_USER=${JENKINS_API_USER:-}
JOB_NAME=automateit
JOB_AUTH_TOKEN=thisismysecrettoken

curl -u "${API_USER}:${API_TOKEN}" $JENKINS_URL/job/$JOB_NAME/build?token=$JOB_AUTH_TOKEN


#!groovy

node {
    def result = sh returnStatus: true, script: 'ansible-lint install_backend.yml && ansible-lint install_frontend.yml'
    if ( result != 0 ) {
        currentBuild.result = 'FAILURE'
        sh "exit ${result}"
    }
}


#!/bin/bash

# bash ./validate-Jenkinsfile.sh

if [ $? -ne 0 ]; then
    echo " >>> Jenkinssfile did not validate"
    exit 1
else
    echo " >>> Jenkinsfile validated"
fi

JENKINS_URL=http://192.168.3.10:8080

status_code=$(curl -k -X POST -s -u "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" -w "%{http_code}" $JENKINS_URL/job/autojob/build)

if [ x"$status_code" == x"201" ]; then
    echo " >>> Job launched"
    exit 0
else
    echo " >>> Job launch failed with code: ${status_code}"
    exit 1
fi

